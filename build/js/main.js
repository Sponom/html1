/*
 * Custom
 */
(function () {
    var burgerBtn = document.querySelector('.header__nav-burger'),
        header = document.querySelector('.header'),
        logo = document.querySelector('.header__logo'),
        overlay = document.querySelector('.header__overlay');
    burgerBtn.onclick = function () {
        this.classList.toggle('header__nav-burger_cross');
        header.classList.toggle('header_opened');
        overlay.classList.toggle('header__overlay_show');
        logo.classList.toggle('header__logo_show');
    }
})();